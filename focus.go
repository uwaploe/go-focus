// Package focus parses the pseudo-NMEA data record from the FOCUS vehicle.
package focus

import (
	"fmt"
	"strconv"
	"strings"

	nmea "bitbucket.org/mfkenney/go-nmea"
	"github.com/pkg/errors"
)

var dataKeys map[string]string = map[string]string{
	"DV": "depth",
	"A1": "altimeter1",
	"A2": "altimeter2",
	"PV": "pitch",
	"RV": "roll",
	"CL": "cableLength",
	"CS": "cableSpeed",
	"CT": "cableTension",
}

// ParseRecord parses the PC Client String record from the FOCUS towbody
// into a map.
func ParseRecord(raw string) (map[string]float32, error) {
	rec := make(map[string]float32)
	s, err := nmea.ParseSentence([]byte(raw))
	if err != nil {
		return rec, errors.Wrap(err, "NMEA parse")
	}

	var x float64
	for _, field := range s.Fields {
		if len(field) < 3 {
			continue
		}
		key := dataKeys[field[0:2]]
		if key != "" {
			if field[3:4] == "-" {
				parts := strings.Split(field[2:], "-")
				x, err = strconv.ParseFloat(parts[0], 32)
			} else {
				x, err = strconv.ParseFloat(field[2:], 32)
			}
			if err != nil {
				//return rec, errors.Wrapf(err, "parsing %q", field)
				continue
			}
			rec[key] = float32(x)
		}
	}
	return rec, nil
}

// ToLci90 outputs the winch data from the PC Client String in LCI-90
// Protocol 2 format.
func ToLci90(rec map[string]float32) string {
	return fmt.Sprintf("**00 %7.f 0 %7.f 0 %7.2f 0\r\n",
		rec["cableTension"],
		rec["cableLength"],
		rec["cableSpeed"])
}
