package focus

import "testing"

func TestParse(t *testing.T) {
	input := "$PMACA,DV000.42,A102.38,PV08.88,RV00.81,CL0000,CS00.00,Y20*46"
	expect := map[string]float32{
		"depth":       0.42,
		"altimeter1":  2.38,
		"pitch":       8.88,
		"roll":        0.81,
		"cableLength": 0,
		"cableSpeed":  0,
	}
	rec, err := ParseRecord(input)
	if err != nil {
		t.Fatal(err)
	}

	for k, v := range expect {
		if v != rec[k] {
			t.Errorf("Bad value for %q; expected %f, got %f", k,
				v, rec[k])
		}
	}
}

func TestNegParse(t *testing.T) {
	input := "$PMACA,DV015.88,A107.25,PV-00.53,RV00.17,CL0040,CS00.00,Y20*65"
	expect := map[string]float32{
		"depth":       15.88,
		"altimeter1":  7.25,
		"pitch":       -0.53,
		"roll":        0.17,
		"cableLength": 40,
		"cableSpeed":  0,
	}
	rec, err := ParseRecord(input)
	if err != nil {
		t.Fatal(err)
	}

	for k, v := range expect {
		if v != rec[k] {
			t.Errorf("Bad value for %q; expected %f, got %f", k,
				v, rec[k])
		}
	}
}

func TestBadRecord(t *testing.T) {
	input := "$PMACA-000.07,PV14.52,RV01.55,CL0002,CS00.00,Y20*2F"
	expect := map[string]float32{
		"depth":       0,
		"pitch":       14.52,
		"roll":        1.55,
		"cableLength": 2,
		"cableSpeed":  0,
	}
	rec, err := ParseRecord(input)
	if err != nil {
		t.Fatal(err)
	}

	for k, v := range expect {
		if v != rec[k] {
			t.Errorf("Bad value for %q; expected %f, got %f", k,
				v, rec[k])
		}
	}
}

func TestLci90(t *testing.T) {
	input := "$PMACA,DV000.42,A102.38,PV08.88,RV00.81,CL0000,CS00.00,Y20*46"
	expect := "**00       0 0       0 0    0.00 0\r\n"
	rec, err := ParseRecord(input)
	if err != nil {
		t.Fatal(err)
	}
	lci := ToLci90(rec)
	if lci != expect {
		t.Errorf("Bad output; expected %q, got %q", expect, lci)
	}
}
